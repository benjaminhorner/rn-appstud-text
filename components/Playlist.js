/**
 * Playlist component
 * Displays a list of playlists fetched from an API
 */

import React from "react";
import {
  StyleSheet,
  SafeAreaView,
  FlatList,
  ActivityIndicator
} from "react-native";
import Api from "../shared/Api";
import Styles from "../shared/Styles";
import { PlaylistItem } from "./PlaylistItem";

export class Playlist extends React.Component {
  /**
   * Compoent Life cycle
   */
  constructor({ props }) {
    super(props);
    this.state = { isLoading: true };
  }
  componentDidMount() {
    Api.getPlaylistFromApiAsync().then(response => {
      this.setState({
        playlistDataSource: response.playlists.items,
        isLoading: false
      });
    });
  }

  /**
   * Render
   */
  render() {
    const { navigation } = this.props;
    if (this.state.isLoading) {
      return (
        <SafeAreaView style={styles.container}>
          <ActivityIndicator size="large" />
        </SafeAreaView>
      );
    } else {
      return (
        <SafeAreaView style={styles.container}>
          <FlatList
            data={this.state.playlistDataSource}
            renderItem={({ item, index, separators }) => (
              <PlaylistItem
                item={item}
                index={index}
                separators={separators}
                navigation={navigation}
              />
            )}
            keyExtractor={item => item.id}
            numColumns={2}
          />
        </SafeAreaView>
      );
    }
  }
}

// Wrap and export props for navigation Hook
export default function(props) {
  const navigation = useNavigation();

  return <Playlist {...props} navigation={navigation} />;
}

/**
 * Compoeznt View Styles
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Styles.BACKGROUND_COLOR,
    justifyContent: "center"
  }
});
