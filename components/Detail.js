import React from "react";
import {
  StyleSheet,
  FlatList,
  SafeAreaView,
  ActivityIndicator,
  View
} from "react-native";
import { useRoute } from "@react-navigation/native";
import { DetailListItem } from "./DetailListItem";
import Styles from "../shared/Styles";
import Api from "../shared/Api";
import { DetailsListHeader } from "./DetailListHeader";

export class Detail extends React.Component {
  /**
   * Life cycle
   */
  constructor({ props }) {
    super(props);
    this.state = {
      isLoading: true
    };
  }
  componentDidMount() {
    const { item } = this.props.route.params;
    this.setState({
      item: item
    });

    const itemId = item.id;
    Api.getPlaylistDetailsFromApiAsync(itemId).then(response => {
      this.setState({
        playlistDataSource: response.tracks.items,
        isLoading: false
      });
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <FlatList
          data={this.state.isLoading ? [{}] : this.state.playlistDataSource}
          renderItem={({ item, index, separators }) =>
            this.state.isLoading ? (
              <View style={styles.spinner}>
                <ActivityIndicator size="large" />
              </View>
            ) : (
              <DetailListItem
                item={item}
                index={index}
                separators={separators}
              />
            )
          }
          keyExtractor={(item, index) => "key" + index}
          numColumns={1}
          ListHeaderComponent={<DetailsListHeader item={this.state.item} />}
          stickyHeaderIndices={[0]}
        />
      </SafeAreaView>
    );
  }
}

// Wrap and export props for navigation Hook
export default function(props) {
  const route = useRoute();

  return <Detail {...props} route={route} />;
}

const styles = StyleSheet.create({
  header: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start"
  },
  headerItem: {
    flex: 2
  },
  container: {
    flex: 1,
    backgroundColor: Styles.BACKGROUND_COLOR
  },
  text: {
    color: Styles.TEXT_COLOR
  },
  spinner: {
    flex: 1,
    justifyContent: "center",
    paddingTop: 100
  }
});
