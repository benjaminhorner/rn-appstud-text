/**
 * PlaulistIOtem represents a component rendered in the Playlist component view
 * It displays a clickacble image that routes to the detail view of the playlist
 */

import React from "react";
import { TouchableHighlight, Image, Dimensions } from "react-native";
import Navigation from "../shared/Navigation";
import { useNavigation } from "@react-navigation/native";

const imageAspectRatio = Dimensions.get("window").width / 2;

export class PlaylistItem extends React.Component {
  constructor({ props }) {
    super(props);
  }

  render() {
    return (
      <TouchableHighlight
        onPress={() => this._onPress(this.props.item)}
        onShowUnderlay={this.props.separators.highlight}
        onHideUnderlay={this.props.separators.unhighlight}
      >
        <Image
          style={{
            width: imageAspectRatio,
            height: imageAspectRatio,
            backgroundColor: "#fff"
          }}
          source={{
            uri:
              this.props.item.images.length > 0
                ? this.props.item.images[0].url
                : null
          }}
        />
      </TouchableHighlight>
    );
  }

  _onPress(item) {
    this.props.navigation.navigate(Navigation.DETAIL, {
      item: item
    });
  }
}

// Wrap and export props for navigation Hook
export default function(props) {
  const navigation = useNavigation();

  return <PlaylistItem {...props} navigation={navigation} />;
}
