/**
 * DetailListItem represents a component rendered in the Detail component view
 * It displays a clickable song title that plays the given song
 */

import React from "react";
import { TouchableHighlight, Text, StyleSheet } from "react-native";
import Styles from "../shared/Styles";

export class DetailListItem extends React.Component {
  constructor({ props }) {
    super(props);
  }

  render() {
    return (
      <TouchableHighlight
        onPress={() => this._onPress(this.props.item)}
        onShowUnderlay={this.props.separators.highlight}
        onHideUnderlay={this.props.separators.unhighlight}
      >
        <Text
          style={
            this.props.item.track.preview_url
              ? styles.textDisabled
              : styles.textActive
          }
        >
          {this.props.item.track.name}
        </Text>
      </TouchableHighlight>
    );
  }

  _onPress(item) {
    // TODO: Play Song
  }
}

const styles = StyleSheet.create({
  textActive: {
    color: Styles.TEXT_COLOR,
    paddingVertical: 20,
    paddingHorizontal: 10
  },
  textDisabled: {
    color: Styles.TEXT_COLOR_DISABLED,
    paddingVertical: 20,
    paddingHorizontal: 10
  }
});
