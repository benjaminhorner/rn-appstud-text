/**
 * DetailsListHeader component is rendered at the top of the detail list of a Playlist
 */

import React from "react";
import { Text, View, Image, Dimensions, StyleSheet } from "react-native";
import { useRoute } from "@react-navigation/native";
import Styles from "../shared/Styles";

const imageAspectRatio = Dimensions.get("window").width / 2.3;

export class DetailsListHeader extends React.Component {
  /**
   * Life cycle
   */
  constructor({ props }) {
    super(props);
  }
  render() {
    return (
      <View style={styles.header}>
        <View>
          <Image
            style={{
              width: imageAspectRatio,
              height: imageAspectRatio,
              backgroundColor: Styles.TEXT_COLOR
            }}
            source={{
              uri:
                this.props.item != undefined
                  ? this.props.item.images[0].url
                  : null
            }}
          />
        </View>
        <View style={styles.item}>
          <Text style={styles.text}>
            {this.props.item != undefined ? this.props.item.description : null}
          </Text>
        </View>
      </View>
    );
  }
}
// Wrap and export props for navigation Hook
export default function(props) {
  const route = useRoute();

  return <Detail {...props} route={route} />;
}

const styles = StyleSheet.create({
  header: {
    flex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
    alignItems: "flex-start",
    backgroundColor: Styles.BACKGROUND_COLOR
  },
  text: {
    color: Styles.TEXT_COLOR
  }
});
