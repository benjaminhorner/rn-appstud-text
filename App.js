import React from "react";
import { Playlist } from "./components/Playlist";
import { Detail } from "./components/Detail";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Navigation from "./shared/Navigation";
import Styles from "./shared/Styles";
import { View } from "react-native";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name={Navigation.PLAYLIST}
          component={Playlist}
          options={styles.header}
        />
        <Stack.Screen
          name={Navigation.DETAIL}
          component={Detail}
          options={styles.header}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = {
  header: {
    headerStyle: {
      backgroundColor: Styles.BACKGROUND_COLOR
    },
    headerTintColor: Styles.TEXT_COLOR,
    headerTitleStyle: {
      fontWeight: "bold"
    }
  }
};
