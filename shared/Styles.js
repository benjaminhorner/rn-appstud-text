export default {
  BACKGROUND_COLOR: "#000",
  TEXT_COLOR: "#fff",
  TEXT_COLOR_DISABLED: "#333"
};
