/**
 * Api
 * Centralizes all API calls and returns
 */

import Globals from "../shared/Globals";

export default {
  async getPlaylistFromApiAsync() {
    try {
      let response = await fetch(Globals.BASE_URL + Globals.PLAYLISTS_ENDPOINT);
      let responseJson = await response.json();
      return responseJson;
    } catch (error) {
      return error;
    }
  },

  async getPlaylistDetailsFromApiAsync(itemId) {
    console.log(Globals.BASE_URL + Globals.PLAYLIST_DETAIL_ENDPOINT + itemId);
    try {
      let response = await fetch(
        Globals.BASE_URL + Globals.PLAYLIST_DETAIL_ENDPOINT + itemId
      );
      let responseJson = await response.json();
      return responseJson;
    } catch (error) {
      return error;
    }
  }
};
