export default {
  BASE_URL: "https://afternoon-waters-49321.herokuapp.com/v1/",
  PLAYLISTS_ENDPOINT: "browse/featured-playlists",
  PLAYLIST_DETAIL_ENDPOINT: "playlists/"
};
